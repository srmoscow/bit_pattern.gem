require "test/unit"
require File.dirname(__FILE__) + '/test_helpers'
require File.dirname(__FILE__) + '/../lib/wide_bit_pattern'


class WideBitPatternTest < Test::Unit::TestCase     #:nodoc:



    def test_individual_bit_setting
        positions = [0,10,31,32,63,70,150,900]
        wbp = WideBitPattern.new
        positions.each do | pos |
            wbp.set_bit(pos)
        end 
        
        positions.each do | pos |
            assert_true wbp.bit_set?(pos), "Bit position #{pos} expected to be set, but is not"
        end 
        
        assert_equal positions, wbp.to_a
    end
    
    
    
    def test_setting_and_unsetting_bits
        wbp = WideBitPattern.new
        wbp.set_bit(124)
        wbp.set_bit(316)
        wbp.set_bit(414)
        wbp.unset_bit(315)
        wbp.unset_bit(316)
        assert_true     wbp.bit_set?(124)
        assert_false    wbp.bit_set?(315)
        assert_false    wbp.bit_set?(316)
        assert_true     wbp.bit_set?(414)
    end
    
    
    def test_width
        wbp = WideBitPattern.new
        assert_equal 32, wbp.width, "When no bits set, expected width of 32; got #{wbp.width}"
        
        wbp.set_bit(31)
        assert_equal 32, wbp.width, "When bit 31 set, expected width of 32; got #{wbp.width}"
        
        wbp.set_bit(32)
        assert_equal 64, wbp.width, "When bit 32 set, expected width of 64; got #{wbp.width}"
        
        wbp.set_bit(100)
        assert_equal 128, wbp.width, "When bit 100 set, expected width of 128; got #{wbp.width}"        
    end
    
    
    def test_to_s
        wbp = WideBitPattern.new
        wbp.set_bit(10)
        wbp.set_bit(20)
        wbp.set_bit(30)
        wbp.set_bit(40)
        wbp.set_bit(50)
        wbp.set_bit(60)
        assert_equal '0001000000000100000000010000000001000000000100000000010000000000', wbp.to_s
    end
  
  

    
    def test_to_hex
        wbp = WideBitPattern.new('1010111110101111101011111010111110101111')
        assert_equal 64, wbp.width
        assert_equal '0x000000afafafafaf', wbp.to_hex
        assert_equal '000000afafafafaf', wbp.to_hex(false)
    end
    
    
    def test_intializing_with_binary_string
        wbp = WideBitPattern.new('1111100000111110000011111000001111100000111110000011111000001111100000')
        expected = [5,6,7,8,9,15,16,17,18,19,25,26,27,28,29,35,36,37,38,39,45,46,47,48,49,55,56,57,58,59,65,66,67,68,69]
        assert_equal expected, wbp.to_a
    end
    
    

    def test_initializing_with_bit_pattern
        bp = BitPattern.new('1010111100110010')
        wbp = WideBitPattern.new(bp)
        assert_equal [1,4,5,8,9,10,11,13,15], wbp.to_a
    end
    
    def test_intitalizing_with_a_bit_pattern_exactly_32_bytes_long
        wbp = BitPattern.new('11000001111100000111110000011111')
        assert_equal '11000001111100000111110000011111', wbp.to_s
    end
    
    
    
    def test_initializing_with_wide_bit_pattern
        wbp1 = WideBitPattern.new('1111100000111110000011111000001111100000111110000011111000001111100000')
        wbp2 = WideBitPattern.new(wbp1)
        expected = [5,6,7,8,9,15,16,17,18,19,25,26,27,28,29,35,36,37,38,39,45,46,47,48,49,55,56,57,58,59,65,66,67,68,69]
        assert_equal expected, wbp2.to_a
    end
    
    
    
    def test_initializing_with_hex_string
        wbp = WideBitPattern.new('0x5c0f00007c')
        assert_equal '0x0000005c0f00007c', wbp.to_hex
    end
    
    
    def test_initializing_with_hex_string_sets_expected_bits
        wbp = WideBitPattern.new('0xc3000000af')
        assert_equal [0,1,2,3,5,7,32,33,38,39], wbp.to_a
    end
    
    
    
    def test_intializing_with_hex_string_exactly_eight_byes_long
        wbp = WideBitPattern.new('0XFEDCBA98')
        assert_equal '0xfedcba98', wbp.to_hex
    end
    
    
    
    
    def test_to_a
        wbp = WideBitPattern.new
        wbp.set_bit(10)
        wbp.set_bit(129)
        wbp.set_bit(68)
        wbp.set_bit(14)
        assert_equal [10, 14, 68, 129], wbp.to_a
    end
    
    
    def test_bit_set_on_a_bit_wider_than_the_current_width
        wbp = WideBitPattern.new('1011')
        assert_equal 32, wbp.width
        assert_false wbp.bit_set?(128)
        assert_false wbp.bit_set?(32)
    end
    
    
    
    
    def test_private_method_get_array_index_from_position
        position_indexes = { 0 => 0,  1 => 0, 10 => 0, 31 => 0, 
                            32 => 1, 44 => 1, 63 => 1,
                            64 => 2, 80 => 2, 95 => 2,
                            96 => 3}
        
        
        wbp = WideBitPattern.new
        position_indexes.each do |pos, expected_index|
            calculated_index = wbp.send(:get_array_index_from, pos)
            assert_equal expected_index, wbp.send(:get_array_index_from, pos),
                        "Expected index #{expected_index} for position #{pos}: got #{calculated_index}"
        end
    end
    
    
    
    def test_private_method_get_array_index_and_offset
            wbp = WideBitPattern.new
            results = { 0 => [0,0],  1 => [0,1],  31 => [0,31],  32 => [1,0], 33 => [1,1] }
            results.each do |pos, expected_result |
                expected_index, expected_offset = expected_result
                index, offset = wbp.send(:get_array_index_and_offset_from, pos)
                assert_equal expected_index, index, "Expected index #{expected_index} for position #{pos}: got #{index}"
                assert_equal expected_offset, offset, "Expected index #{expected_offset} for position #{pos}: got #{offset}"
            end
        end
            
            
            
            
end