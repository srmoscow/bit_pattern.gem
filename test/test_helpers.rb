
class Test::Unit::TestCase
    def assert_true(condition, message = nil)           #:nodoc:
        assert condition, message
    end

    def assert_false(condition, message = nil)          #:nodoc:
        assert_equal false, condition, message
    end
    
end