require "test/unit"
require File.dirname(__FILE__) + '/test_helpers'
require File.dirname(__FILE__) + '/../lib/bit_pattern'






class BitPatternTest < Test::Unit::TestCase               #:nodoc:

    # def setup
    #     # setup code here
    # end


    def test_vaies_for_individual_bits_set
        position_values = { 
            0   => 1,
            1   => 2,
            2   => 4,
            3   => 8,
            4   => 16,
            5   => 32,
            6   => 64,
            7   => 128,
            8   => 256,
            9   => 512,
            10  => 1_024,
            11  => 2_048,
            12  => 4_096,
            13  => 8_192,
            14  => 16_384,
            15  => 32_768,
            16  => 65_536,
            17  => 131_072,
            18  => 262_144,
            19  => 524_288,
            20  => 1_048_576,
            21  => 2_097_152,
            22  => 4_194_304,
            23  => 8_388_608,
            24  => 16_777_216,
            25  => 33_554_432,  
            26  => 67_108_864,
            27  => 134_217_728,
            28  => 268_435_456,
            29  => 536_870_912,
            30  => 1_073_741_824,
            31  => 2_147_483_648
            
        }
        position_values.keys.sort.each do | pos |
          
            bp = BitPattern.new
            bp.set_bit(pos)
        
            assert_equal position_values[pos], bp.value, "position #{pos}: expecting a value of #{position_values[pos]}, got #{bp.value}"
            assert_true bp.bit_set?(pos), "Expecting position #{pos} to be set"
        end
     end
     
     
     
     def test_initialising_with_string_gt_32_bytes
        err = assert_raise ArgumentError do
            bp = BitPattern.new('101011001100111100001010110001010000100')
        end
        assert_equal 'String passsed to BitPatern::new must be 32 characters or less', err.message
     end
     
     
     
     def test_initializing_with_string_containing_invalid_characters
        err = assert_raise ArgumentError do
            bp = BitPattern.new('  1100')
        end
        assert_equal 'Unrecognised value passed to BitPattern::new:  >>  1100<<', err.message    
        
        err = assert_raise ArgumentError do
            bp = BitPattern.new('00AF')
        end
        assert_equal 'Unrecognised value passed to BitPattern::new:  >>00AF<<', err.message    
        
     end
     
     
     def test_initializing_with_a_hex_string
     
         bp = BitPattern.new('0xef')
         assert_equal '11101111', bp.to_s
         
         bp = BitPattern.new('0X0000af')
         assert_equal '10101111', bp.to_s
     end


     
    
        
     def test_intialising_with_string_of_1s_and_0s
         bp = BitPattern.new('1010100')
         assert_equal 84, bp.value
         assert_false   bp.bit_set?(30)
         assert_true    bp.bit_set?(6)
         assert_false   bp.bit_set?(5)
         assert_true    bp.bit_set?(4)
         assert_false   bp.bit_set?(3)
         assert_true    bp.bit_set?(2)
         assert_false   bp.bit_set?(1)
         assert_false   bp.bit_set?(0)
     end
     
     
     def test_initializing_with_another_bit_pattern
         other_bit_pattern = BitPattern.new('1011')
         bp = BitPattern.new(other_bit_pattern)
         assert_equal 11, bp.value
         assert_true    bp.bit_set?(0)
         assert_true    bp.bit_set?(1)
         assert_false   bp.bit_set?(2)
         assert_true    bp.bit_set?(3)
         assert_false   bp.bit_set?(4)
     end
     
     
     def test_to_s
         bp = BitPattern.new('1001')
         bp.set_bit(2)
         assert_equal '1101', bp.to_s
     end
     
     def test_to_s_padded
         bp = BitPattern.new('1001')
         bp.set_bit(2)
         assert_equal '00000000000000000000000000001101',bp.to_s_padded
     end
     
     
     
     def test_setting_bits_that_are_set
         bp = BitPattern.new('101111')
         bp.set_bit(3)
         assert_equal '101111', bp.to_s
     end
     
     def test_setting_bits_that_are_unset
         bp = BitPattern.new('101111')
         bp.set_bit(4)
         assert_equal '111111', bp.to_s
     end
     
     
     def test_unsetting_bits_that_are_set
         bp = BitPattern.new('1110011000011')
         val_1 = bp.unset_bit(1)
         assert_equal 7361, val_1
         val_2 = bp.unset_bit(7)
         assert_equal 7233, val_2
         assert_equal '1110001000001', bp.to_s
     end
     
     
     def test_unsetting_bits_that_are_not_set
         bp = BitPattern.new('10100')
         new_value = bp.unset_bit(0)
         assert_equal '10100', bp.to_s
         assert_equal 20, new_value
     end
     
     
     def test_to_hex
         bp = BitPattern.new('10101111')
         assert_equal '0x000000af', bp.to_hex
     end
     
     def test_to_hex_without_leading_0x
         bp = BitPattern.new('10101111')
         assert_equal '000000af', bp.to_hex(false)
     end
     
     
     
     def test_to_array_on_empty_bit_pattern
         bp = BitPattern.new
         assert_equal [], bp.to_a
     end
     
     
     
     def test_to_array
         bp = BitPattern.new
         bp.set_bit(30)
         bp.set_bit(4)
         bp.set_bit(22)
         assert_equal [4, 22, 30], bp.to_a
     end
     
    


end