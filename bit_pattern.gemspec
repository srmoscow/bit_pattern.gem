Gem::Specification.new do |s|
  s.name = %q{bit_pattern}
  s.version = "1.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Stephen Richards"]
  s.date = %q{2010-02-05}
  #s.default_executable = %q{mindy}
  s.description = %q{Classes to manipulate bit patterns; set / unset bits, retrieve as hex or binary strings.}
  s.email = ["stephen@stephenrichards.eu"]
  #s.executables = ["mindy"]
  s.files = [
            "lib/bit_pattern.rb", 
            "lib/wide_bit_pattern.rb", 
            "test/bit_pattern_test.rb", 
            "test/wide_bit_pattern_test.rb", 
            "test/test_helpers.rb",
            "README"
        ]
  s.has_rdoc = true
  s.homepage = %q{http://www.stephenrichards.eu}
  s.rdoc_options = ["--inline-source", "--include=doc", "--charset=UTF-8", "--exclude=.yml", "--exclude=test/*"]
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.0}
  s.summary = %q{Bit Pattern manipulation class.}
  s.test_files = [
            "test/bit_pattern_test.rb",
            "test/wide_bit_pattern_test.rb",
            "test/test_helpers.rb"
        ]

 if s.respond_to? :specification_version then
   current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
   s.specification_version = 2
 
 #  if Gem::Version.new(Gem::RubyGemsVersion) >= Gem::Version.new('1.2.0') then
 #    s.add_runtime_dependency(%q<sinatra>, [">= 0.9.0.4"])
 #  else
 #    s.add_dependency(%q<sinatra>, [">= 0.9.0.4"])
 #  end
 #else
 #  s.add_dependency(%q<sinatra>, [">= 0.9.0.4"])
 end
end