#:main: README


# Class to facilitate the setting and unsetting of a 32-bit bit pattern


class BitPattern
    
    attr_reader :value
    
    
    # Instantiates a BitPattern object, optionally with a predefined pattern.
    #
    # *Parameters:*
    # 
    # * param_value: either:
    #   * another bit_pattern object, in which case this object is instantiated with the same bit pattern
    #     as the parameter passed in, or 
    #   * a string of up to 32 characters, each of which must be 1 or 0. If less than 32 characters passed in, then the last 
    #     character will be bit zero, and unspecified positions on the left will be set to zero.
    #   * a hexadecimal string, in upper or lower case, prefixed by '0x' or '0X'.  '0x23AF', '0X23af', '0x23af' are all valid.
    #
    # If no parameter is passed in, then a bit pattern of all zeroes is created.
    #
    def initialize(param_value = nil)
        @value = 0
        initialize_value(param_value) if param_value
    end
    
    
    
    
    # Returns width of bit pattern (currently always 32)
    #
    def self.width
        32
    end
    
    
    # Sets the bit in the specified position
    #
    # *Parameters:*
    # * position: The position of the bit to set, in the range 0 to 31
    #
    # *Returns:* the value of the bit pattern after the bit has been set
    #
    def set_bit(position)
        mask_value = get_mask_value(position)
        @value = @value | mask_value
    end
    
    # Unsets the bit in the specified position
    #
    # *Parameters:*
    # * position: The position of the bit to set, in the range 0 to 31
    #
    # *Returns:* the value of the bit pattern after the bit has been unset
    #
    def unset_bit(position)
        if self.bit_set?(position)
            mask_value = get_mask_value(position)
            @value = @value ^ mask_value
        end
        @value
    end
    
    
    
    # Interrogates the setting of a bit position
    # 
    # *Parameters:*
    # * position: the bit position that is to be interrogated, in the range 0 to 31
    #
    # *Returns:* true if the bit is set, otherwise false
    #
    def bit_set?(position)
        mask_value = get_mask_value(position)
        return true if @value & mask_value == mask_value
        return false
        
    end
    
    
    
    
    # Returns the bit pattern as a string of up to 32 bytes of 1s and 0s.  Leading zeros are not included in the returned string.
    #
    def to_s
        stringify(false)
    end

    
    
    
    # Returns the bit pattern as an 32-byte pattern of 1s and 0s
    #
    def to_s_padded
        stringify(true)
    end
    
    
           
           
           
    # Returns an eight-character zero-padded hexadecimal string.
    #
    # *Parameters:*
    # * leading_0x_required: false if the leading '0x' is to be omitted from the result.  Defaults to true.
    #
    # *Example:*
    #
    #      bp = BitPattern.new('10101111')
    #      bp.to_hex                # => '0x000000af'
    #      vp.to_hex(false)         # => '000000af'
    #
    def to_hex(leading_0x_required = true)
        hex = "%08x" % @value
        hex = '0x'+hex if leading_0x_required
        hex
    end
        
    
    # Returns an array of set bit positions in ascending numerical order.
    #
    # *Example:*
    #
    #     bp = BitPattern.new
    #     bp.set_bit(30)
    #     bp.set_bit(4)
    #     bp.set_bit(22)
    #     bp.to_a               # => [4, 22, 30]
    #
    def to_a
        array = []
        (0..31).each do | bit |
            array << bit if self.bit_set?(bit)
        end 
        array
    end
        
  
    
    private
    
    def stringify(first_one_seen = false)                               #:nodoc:
        i = BitPattern.width - 1
        str = ''
        while i > -1 do
            if self.bit_set?(i)
                first_one_seen = true
                str += '1'
            else
                str += '0' if first_one_seen
            end
            i -= 1
        end
        str
    end
    
    
    
    def initialize_value(param_value)                                   #:nodoc:
        if param_value.is_a?(BitPattern)
            initialize_value_from_bit_pattern(param_value)
        elsif param_value.is_a?(String)
            initialize_value_from_string(param_value)
        else
            raise ArgumentError.new("Parameter passed to BitPatter::new must be String or BitPattern: was #{param_value.class}")
        end
    end
    
    
    
    def initialize_value_from_bit_pattern(param_value)                  #:nodoc:
        @value = param_value.value
    end
    
    
    def initialize_value_from_string(param_value)                       #:nodoc:
        if binary_string?(param_value)
            initialize_value_from_binary_string(param_value)
        elsif hexadecimal_string?(param_value)
            initialize_value_from_hexadecimal_string(param_value)
        else
            raise ArgumentError.new("Unrecognised value passed to BitPattern::new:  >>#{param_value}<<")
        end
    end
       
    
    def hexadecimal_string?(param_value)            #:nodoc:
        return true if param_value =~ /^(0x|0X)[0-9a-fA-F]*$/
        return false
    end
    
    
    
       
       
    def initialize_value_from_hexadecimal_string(param_value)           #:nodoc:
        @value = param_value.to_i(16)
    end
    
    
            
    
    
    
    
    def initialize_value_from_binary_string(param_value)                #:nodoc:
       if param_value.size > BitPattern.width
            raise ArgumentError.new("String passsed to BitPatern::new must be #{BitPattern.width} characters or less")
        end
        
        param_value.reverse!                        # so that we can work in the order bit 0, bit 1, bit 2...
        i = 0
        param_value.each_byte do |byte|
            self.set_bit(i) if byte == 49           # 49 is the ascii value for character '1'
            i += 1
        end
    end
    
    
    
    def binary_string?(param_value)                                     #:nodoc:
        return true if param_value =~ /^[0-1]+$/
        return false
    end
    
    
    
    def get_mask_value(position)                                        #:nodoc:
        validate_bit_position(position)
        if position == 0
            mask_value = 1
        else
            mask_value = (2 ** position)
        end
        mask_value
    end
        
        
        
        
    def validate_bit_position(position)                                 #:nodoc:
        raise "Invalid bit postion: #{position}" if position < 0  || position > 31
    end

end                