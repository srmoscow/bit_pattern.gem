require File.dirname(__FILE__) + '/bit_pattern'


# A class to create, manipulate and store bit patterns of any width.  WideBitPatterns are initially created 32 bits
# wide, and expanded in chunks of 32 bits as necessary.

class WideBitPattern < BitPattern
    
    attr_reader :bit_patterns
    
    # Instantiates a WideBitPattern object, optionally with a predefined pattern.
    #
    # *Parameters:*
    # 
    # * param_value: Optional.  If supplied, then one of the following:
    #   * another wide_bit_pattern object, in which case this object is instantiated with the same bit pattern
    #     as that of the passed-in object
    #   * a bit_pattern object, in which case this object is instantiated with the same bit pattern as that of the 
    #     passed-in object
    #   * a string of up to 32 characters, each of which must be 1 or 0. If less than 32 characters passed in, then the last 
    #     character will be bit zero, and unspecified positions on the left will be set to zero. 
    #   * a hexadecimal string, in upper or lower case, prefixed by '0x' or '0X'.  '0x23AF', '0X23af', '0x23af' are all valid.
    #
    # If no parameter is passed in, then a 32 bit-wide bit pattern of all zeroes is created.
    #
    def initialize(value = nil)
        @bit_patterns = []
        if value
            initialize_from(value)
        else
            @bit_patterns << BitPattern.new
        end
    end
    
    # Sets the bit in the specified position
    #
    # *Parameters:*
    # * position: The position of the bit to set
    #
    # *Returns:* nil
    #
    def set_bit(position)
        array_index, offset = get_array_index_and_offset_from(position)
        extend_bit_patterns_array_if_required(array_index)
        @bit_patterns[array_index].set_bit(offset)
        nil
    end
    
    
    # Unsets the bit in the specified position
    #
    # *Parameters:*
    # * position: The position of the bit to set.
    #
    # *Returns:* nil.
    #    
    def unset_bit(position)
        array_index, offset = get_array_index_and_offset_from(position)
        extend_bit_patterns_array_if_required(array_index)
        @bit_patterns[array_index].unset_bit(offset)    
        nil
    end
    

    # Interrogates the setting of a bit position
    # 
    # *Parameters:*
    # * position: the bit position that is to be interrogated.
    #
    # *Returns:* true if the bit is set, otherwise false
    #
    def bit_set?(position)
        return false if position + 1 > self.width
        array_index, offset = get_array_index_and_offset_from(position)
        @bit_patterns[array_index].bit_set?(offset)
    end
    
    
    # Returns an array of set bit positions, in ascending numerical order.
    #
    # *Example:*
    #
    #     wbp = WideBitPattern.new
    #     wbp.set_bit(30)
    #     wbp.set_bit(4)
    #     wbp.set_bit(122)
    #     wbp.to_a               # => [4, 30, 122]
    #    
    def to_a
        array = []
        i = 0
        @bit_patterns.each do |bp|
            multiplier = i * 32
            bp.to_a.each do |position|
                array << position + multiplier
            end
            i += 1
        end
        array
    end
    
    
    # Returns the bit pattern as a string of 1s and 0s.  Leading zeros are not included in the returned string.
    #    
    def to_s
        str = ''
        @bit_patterns.reverse.each do |bp|
            str << bp.to_s_padded
        end
        str
    end
        
        
    
    # Returns width of the bit pattern.  Bit patterns are assigned in 32-bit chunks, so setting any bit in the range 0-31 will
    # result in a bit pattern with a width of 32, setting a bit in the range 32-63 will result in a bit pattern width of 64,
    # and so on.
    #
    def width
        @bit_patterns.size * BitPattern.width
    end
    
    
    # Returns an zero-padded hexadecimal string. The length of the returned string will be the object's width divided by 4.
    #
    # *Parameters:*
    # * leading_0x_required: false if the leading '0x' is to be omitted from the result.  Defaults to true.
    #
    # *Example:*
    #
    #      bp = BitPattern.new('1010111110101111101011111010111110101111')
    #      bp.to_hex                # => '0x000000afafafafaf'
    #      vp.to_hex(false)         # => '000000afafafafaf'
    #
    def to_hex(leading_0x_required = true)
        str = ''
        @bit_patterns.reverse.each do |bp|
            str << bp.to_hex(false)
        end
        str = '0x' + str if leading_0x_required
        str
    end
        
    
    
    
    
    
    private
    def initialize_from(param_value)                    #:nodoc:
        if param_value.is_a?(BitPattern)
            @bit_patterns << param_value
        elsif param_value.is_a?(WideBitPattern)
            @bit_patterns =  param_value.bit_patterns
        elsif binary_string?(param_value)
            initialize_from_binary_string(param_value)
        elsif hexadecimal_string?(param_value)
            initialize_from_hexadecimal_sting(param_value)
        else
            raise ArgumentError.new("Invalid argument passed to WideBitPattern::new: #{value}")
        end
    end
    
    
 
    def initialize_from_hexadecimal_sting(param_value)
        # remove leading 0x
        param_value.sub!(/^0[x|X]/, '')
        remainder = param_value.size % 8
        
        if remainder > 0
            hex_string = param_value[0..remainder -1]
            @bit_patterns.unshift BitPattern.new('0x' + hex_string)
        end
        
        start_ix = remainder
        end_ix = remainder + 8
        while start_ix < param_value.size
            hex_string = '0x' + param_value[start_ix...end_ix]
            @bit_patterns.unshift BitPattern.new(hex_string)
            start_ix += 8
            end_ix += 8
        end
    end
 
  
    
    
   def initialize_from_binary_string(param_value)                           #:nodoc:
        remainder        = param_value.size % BitPattern.width
        
        if remainder > 0
            bit_pattern_string = param_value[0..remainder - 1]
            @bit_patterns.unshift BitPattern.new(bit_pattern_string)
        end
        
        start_ix = remainder
        end_ix  = remainder + BitPattern.width
        while start_ix < param_value.size
            bit_pattern_string = param_value[start_ix...end_ix] 
            @bit_patterns.unshift BitPattern.new(bit_pattern_string)
            start_ix  += BitPattern.width
            end_ix += BitPattern.width
        end
    end
        
    
    def get_array_index_and_offset_from(position)       #:nodoc:
        index = get_array_index_from(position)
        offset = position - (index * BitPattern.width)
        return [index, offset]
    end
        
    
    
    
    
    def get_array_index_from(position)                  #:nodoc:
        index = (position) / BitPattern.width
        return index
        
    end
    
    
    def extend_bit_patterns_array_if_required(array_index)          #:nodoc:
        while @bit_patterns.size - 1 < array_index
            @bit_patterns << BitPattern.new
        end
    end
    
    
end